//------------------------------------------------------------------------------
//          Copyright 2014 RED Engine Games, LLC. All rights reserved.
//
//                  The best method for accelerating a computer
//                     is the one that boosts it by 9.8 m/s2.
//------------------------------------------------------------------------------

#include<android_native_app_glue.h>

//================================================|=============================
//android_main:{                                  |

  void android_main( android_app* state ){
    app_dummy();
  }

//}:                                              |
//================================================|=============================
