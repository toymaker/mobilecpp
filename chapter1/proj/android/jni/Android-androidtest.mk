# vim:et

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

# ==============================================================================
# Choose module name.
# ==============================================================================

#$(warning $(APP_OPTIM))

LOCAL_MODULE          := libAndroidTest
LOCAL_MODULE_FILENAME := libAndroidTest

# ==============================================================================
# Compiler options.
# ==============================================================================

LOCAL_LDLIBS           := -lstdc++ -lc -lm -llog -landroid -ldl -lEGL -lGLESv2
LOCAL_STATIC_LIBRARIES += android_native_app_glue
LOCAL_C_INCLUDES       := include
LOCAL_CPPFLAGS         := -DANDROID
LOCAL_CPPFLAGS         += -std=gnu++11
LOCAL_CPPFLAGS         += -fexceptions
LOCAL_CPPFLAGS         += -ffast-math
ifeq ($(APP_OPTIM),debug)
  LOCAL_CPPFLAGS += -DDEBUG
endif

# ==============================================================================
# GameForge source files.
# ==============================================================================

LOCAL_SRC_FILES := src/main.cpp

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
