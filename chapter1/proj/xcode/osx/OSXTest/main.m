//
//  main.m
//  OSXTest
//
//  Created by Brian Hapgood on 7/28/14.
//  Copyright (c) 2014 Red Engine Games LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
  return NSApplicationMain(argc, argv);
}
